package com.ad.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;

public class CommonUtil
{

    private static final String USER_AGENT = "Mozilla/5.0";

    public static String getCall(String URL)
    {
	String output = "";
	try
	{
	    // create HTTP Client
	    CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	    // Create new getRequest with below mentioned URL
	    HttpGet getRequest = new HttpGet(URL);
	    getRequest.addHeader("accept", "application/json");

	    // Execute your request and catch response
	    HttpResponse response = httpClient.execute(getRequest);

	    // Check for HTTP response code: 200 = success
	    if (response.getStatusLine().getStatusCode() != 200)
	    {
		throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
	    }

	    // Get-Capture Complete application/json/text/xml body response
	    BufferedReader reader = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

	    StringBuffer responseBfr = new StringBuffer();

	    while ((output = reader.readLine()) != null)
	    {
		responseBfr.append(output);
	    }
	    output = responseBfr.toString();

	    reader.close();

	    System.out.println("Get RESPONSE:" + output.toString());
	    httpClient.close();

	} catch (IOException e)
	{
	    e.printStackTrace();
	}
	return output;
    }

    public static String sendPOST(String URL, String requestJson) throws IOException
    {
	String output = "";

	CloseableHttpClient httpClient = HttpClients.createDefault();
	HttpPost httpPost = new HttpPost(URL);
	httpPost.addHeader("User-Agent", USER_AGENT);
	httpPost.addHeader("Content-Type", "application/json");

	// Set payload
	StringEntity entity = new StringEntity(requestJson);
	httpPost.setEntity(entity);

	// Execute post call
	CloseableHttpResponse httpResponse = httpClient.execute(httpPost);

	// Process response
	BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

	String inputLine;
	StringBuffer response = new StringBuffer();

	while ((inputLine = reader.readLine()) != null)
	{
	    response.append(inputLine);
	}
	reader.close();

	output = response.toString();
	httpClient.close();

	return output;
    }
}
