package com.ad.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.ad.util.ConversionStrategy;

public class JSONMapperUtil
{
    static ObjectMapper objMapper = new ObjectMapper();

    public static String mapException(Exception cx)
    {
	String returnJson = "";
	try
	{
	    ObjectWriter objWriter = objMapper.writerWithType(Exception.class);
	    returnJson = objWriter.writeValueAsString(cx);
	} catch (IOException e)
	{
	    returnJson = "{\"exceptionCode\":\"" + "String MAP Exception" + "\"}";
	}
	return returnJson;
    }

    public static String convertJSONToString(Object object, boolean includeNULL) throws Exception
    {
	String responseStr = "";

	try
	{
	    objMapper.setPropertyNamingStrategy(new ConversionStrategy());
	    if (null == object)
	    {
		responseStr = "";
	    } else
	    {
		if (includeNULL)
		{
		    objMapper.setSerializationInclusion(JsonSerialize.Inclusion.ALWAYS);
		} else
		{
		    objMapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		}

		// This line is added to fix the error: no properties discovered
		// to create BeanSerializer (to avoid exception, disable
		// SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS
		objMapper.disable(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS);

		ObjectWriter objWriter = objMapper.writerWithType(object.getClass());
		responseStr = objWriter.writeValueAsString(object);
	    }

	} catch (Exception ex)
	{
	    throw ex;
	}

	return responseStr;
    }

    public static String formatError(Exception cx, String methodName)
    {

	return (StringUtils.isBlank(cx.getMessage()) ? "Error Occurred" : "");

    }

    public static String convertJSONToString(Object object) throws Exception
    {
	return convertJSONToString(object, false);
    }

    public static <T> T convertStringTOJSON(String jsonString, Class<T> mappedClass) throws Exception
    {

	return convertStringTOJSON(jsonString, mappedClass, false);
    }

    public static <T> T convertStringTOJSON(String jsonString, Class<T> mappedClass, boolean ignoreUnknown)
	    throws Exception
    {
	objMapper.setPropertyNamingStrategy(new ConversionStrategy());
	if (ignoreUnknown)
	{
	    objMapper.disable(Feature.FAIL_ON_UNKNOWN_PROPERTIES);
	    // following lines added to allow unquoted control
	    // characters while parsing the JSON string
	    objMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
	    objMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
	}
	ObjectReader objReader = objMapper.reader(mappedClass);
	return objReader.readValue(jsonString);
    }

    /**
     * This method is used to convert CLOB object to String object
     */
    public static String clobToString(Clob data) throws Exception
    {
	StringBuilder sb = new StringBuilder();
	Reader reader = null;
	BufferedReader br = null;
	try
	{
	    reader = data.getCharacterStream();
	    br = new BufferedReader(reader);

	    String line;
	    while (null != (line = br.readLine()))
	    {
		sb.append(line);
	    }

	} catch (SQLException SQLEx)
	{
	    throw SQLEx;
	} catch (IOException IOEx)
	{
	    throw IOEx;
	} finally
	{
	    try
	    {
		if (null != br)
		{
		    br.close();
		}

	    } catch (IOException ioe)
	    {
	    	ioe.printStackTrace();
	    }

	    try
	    {
		if (null != reader)
		{
		    reader.close();
		}

	    } catch (IOException ioe)
	    {
	    	ioe.printStackTrace();
	    }
	}
	return sb.toString();
    }

}
