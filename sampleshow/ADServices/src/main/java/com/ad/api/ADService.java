package com.ad.api;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.ad.model.AdDetailJson;
import com.ad.model.AdListJson;
import com.ad.model.AdditionalStatus.AddSeverity;
import com.ad.model.Status;
import com.ad.util.JSONMapperUtil;

@Path("/adservice")
public class ADService
{
    public static AdListJson adsList = null;

    // http://localhost:8080/ADServer/RS/adservice/createAd
    /**
     * This method will return create the Ad in a single HashMap in the memory.
     * @param payLoad
     * @return
     * @throws Exception
     */
    @POST
    @Path("/createAd")
    @Produces("application/json")
    @Consumes("application/json")
    public Response createAd(String payLoad) throws Exception
    {
	String resultJson = "";

	try
	{
	    AdDetailJson adJson = JSONMapperUtil.convertStringTOJSON(payLoad, AdDetailJson.class, true);

	    // Do input validation here to make sure required fields values are
	    // present in the input JSON
	    // else throw validation error.

	    // Call the helper service method to add the Ad to the list
	    adsList = new ADServiceHandler().manageAds(adsList, adJson);

	    resultJson = JSONMapperUtil.convertJSONToString(adsList, true);

	} catch (Exception ex)
	{
	    ex.printStackTrace();

	    // Below error object and usage can be enhance to send the
	    // Validation error or any other error in the response.
	    Status status = new Status();
	    status.getAdditionalStatuss().get(0).setServerStatusCode("ERROR_1");
	    status.getAdditionalStatuss().get(0).setStatusDesc(
		    "An Error Occured while creating the Ad, please check the server logs for detailed message!");
	    status.getAdditionalStatuss().get(0).setSeverity(AddSeverity.ERROR);
	    return Response.status(500).entity(status).build();
	}
	return Response.status(200).entity(resultJson).build();
    }

    // http://localhost:8080/ADServer/RS/adservice/getAllAds
    /**
     * This API method will return the JSON list of all the Ad's
     * @return
     * @throws Exception
     */
    @GET
    @Path("/getAllAds")
    @Produces("application/json")
    public Response getAllAds() throws Exception
    {
	String result = "";
	try
	{
	    // Convert the response
	    result = JSONMapperUtil.convertJSONToString(new ADServiceHandler().getAllAds(adsList), true);
	} catch (Exception ex)
	{
	    ex.printStackTrace();
	    // Below error object and usage can be enhance to send the
	    // Validation error or any other error in the response.
	    Status status = new Status();
	    status.getAdditionalStatuss().get(0).setServerStatusCode("ERROR_2");
	    status.getAdditionalStatuss().get(0).setStatusDesc(
		    "An Error Occured while fetching the Ads list, please check the server logs for detailed message!");
	    status.getAdditionalStatuss().get(0).setSeverity(AddSeverity.ERROR);
	    return Response.status(500).entity(status).build();
	}

	return Response.status(200).entity(result).build();
    }

    // http://localhost:8080/ADServer/RS/adservice/getAdsByPartner/{partnername}
    /**
     * This method will return all Ad's for the requested partner
     * @param partnerId
     * @return
     * @throws Exception
     */
    @GET
    @Path("/getAdsByPartner/{partnerId}")
    @Produces("application/json")
    public Response getAdsByPartner(@PathParam("partnerId") String partnerId) throws Exception
    {
	String result = "";
	try
	{
	    if (null != adsList)
	    {
		ArrayList<AdDetailJson> partnerAds = new ADServiceHandler().getAdsByPartner(adsList, partnerId);
		if (null != partnerAds)
		{
		    AdListJson adsList = new AdListJson();
		    adsList.getAdListMap().put(partnerId, partnerAds);
		    result = JSONMapperUtil.convertJSONToString(adsList, true);
		} else
		{
		    throw new Exception("No ad found for the partner:" + partnerId);
		}
	    }
	} catch (Exception ex)
	{
	    ex.printStackTrace();
	    // Below error object and usage can be enhance to send the
	    // Validation error or any other error in the response.
	    Status status = new Status();
	    status.getAdditionalStatuss().get(0).setServerStatusCode("ERROR_3");
	    status.getAdditionalStatuss().get(0).setStatusDesc("An Error Occured while fetching the Ads for partner "
		    + partnerId + ", please check the server logs for detailed message!");
	    status.getAdditionalStatuss().get(0).setSeverity(AddSeverity.ERROR);
	    return Response.status(500).entity(status).build();
	}

	return Response.status(200).entity(result).build();
    }
}