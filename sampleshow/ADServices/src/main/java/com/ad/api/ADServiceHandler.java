package com.ad.api;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.ad.model.AdDetailJson;
import com.ad.model.AdListJson;
import com.ad.util.JSONMapperUtil;

public class ADServiceHandler
{
    /**
     * 
     * @param adsList - List of all adds from all partners
     * @param adJson - Ad to be added to the list
     * @return
     */
    public AdListJson manageAds(AdListJson adsList, AdDetailJson adJson)
    {
	if (adsList == null)
	{
	    adsList = new AdListJson();
	}

	// If Partner exists then add the to the partner ad list.
	if (adsList.getAdListMap().containsKey(adJson.getPartner_id()))
	{
	    // Iterate ads and in-activate the active ad if expired
	    for (AdDetailJson ad : adsList.getAdListMap().get(adJson.getPartner_id()))
	    {
		if (ad.isAd_active())
		{
		    Calendar calendar = Calendar.getInstance();
		    calendar.setTime(ad.getAdStartTime());
		    calendar.add(Calendar.SECOND, ad.getDuration_secs());

		    Date currDate = new Date();
		    if (calendar.getTime().compareTo(currDate) < 0)
		    {
			// Ad expired
			ad.setAd_active(false);
		    }
		}
	    }
	    adJson.setAdStartTime(new Date());
	    adsList.getAdListMap().get(adJson.getPartner_id()).add(adJson);
	} else
	{// Add new partner and ad, also activate the ad and set start time
	    ArrayList<AdDetailJson> adList = new ArrayList<AdDetailJson>();
	    adJson.setAd_active(true);
	    // Set ad start time
	    adJson.setAdStartTime(new Date());
	    adList.add(adJson);
	    adsList.getAdListMap().put(adJson.getPartner_id(), adList);
	}

	return adsList;
    }
    
    /**
     * Returns the Ad list
     * @param adsList
     * @return
     */
    public AdListJson getAllAds(AdListJson adsList)
    {
	return adsList;
    }
    
    
    public ArrayList<AdDetailJson> getAdsByPartner(AdListJson adsList, String partnerId)
    {
	if (adsList.getAdListMap().containsKey(partnerId))
	{
	    return adsList.getAdListMap().get(partnerId);
	} else
	{
	    return null;
	}

    }

}