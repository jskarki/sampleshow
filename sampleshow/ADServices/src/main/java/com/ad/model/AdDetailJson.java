package com.ad.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement
@XmlType(name = "AdDetailJson", namespace = "ad")
public class AdDetailJson implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    // unique_string_representing_partner
    @JsonProperty
    private String partner_id;

    // int_representing_campaign_duration_in_seconds_from_now
    @JsonProperty
    private int duration_secs;

    // string_of_content_to_display_as_ad
    @JsonProperty
    private String ad_content;

    // ad status if active or inactive
    @JsonProperty
    boolean ad_active = false;
    
    // Time when the ad started
    //@Ignore
    @JsonProperty
    Date adStartTime;
    
    public AdDetailJson()
    {
	
    }
    
    public AdDetailJson(String partner_id, int duration_secs, String ad_content)
    {
	this.partner_id = partner_id;
	this.duration_secs = duration_secs;
	this.ad_content = ad_content;
    }

    public String getPartner_id()
    {
	return partner_id;
    }

    public void setPartner_id(String partner_id)
    {
	this.partner_id = partner_id;
    }

    public String getAd_content()
    {
	return ad_content;
    }

    public void setAd_content(String ad_content)
    {
	this.ad_content = ad_content;
    }

    public boolean isAd_active()
    {
	return ad_active;
    }

    public void setAd_active(boolean ad_active)
    {
	this.ad_active = ad_active;
    }

    public int getDuration_secs()
    {
	return duration_secs;
    }

    public void setDuration_secs(int duration_secs)
    {
	this.duration_secs = duration_secs;
    }

    public Date getAdStartTime()
    {
        return adStartTime;
    }

    public void setAdStartTime(Date adStartTime)
    {
        this.adStartTime = adStartTime;
    }

}