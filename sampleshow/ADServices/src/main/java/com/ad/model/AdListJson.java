package com.ad.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement
@XmlType(name = "AdListJson", namespace = "ad")
public class AdListJson implements Serializable
{
    private static final long serialVersionUID = 1L;

    @JsonProperty
    private Map<String, ArrayList<AdDetailJson>> adListMap = new HashMap<String, ArrayList<AdDetailJson>>();
    
    public AdListJson()
    {
	
    }
    
    public Map<String, ArrayList<AdDetailJson>> getAdListMap()
    {
	if(null == adListMap)
	{
	    adListMap = new HashMap<String, ArrayList<AdDetailJson>>();   
	}
	return adListMap;
    }

    public void setAdListMap(Map<String, ArrayList<AdDetailJson>> adListMap)
    {
	this.adListMap = adListMap;
    }

}