package com.ad.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonValue;

/**
 * 
 * 
 */
@XmlRootElement
@XmlType(name = "AdditionalStatus", namespace = "addstatus")
public class AdditionalStatus implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * Response Status Code.
     * Valid values depend on context. See specific API documentation site.
     * Business logic may be written based on the values of this field.
     * (Required)
     * 
     */
    @JsonProperty("statusCode")
    private Integer statusCode;
    /**
     * Server Status Code.
     * The value placed here is used to allow the client to display a status code to the user, or log appropriately for troubleshooting purposes.
     * No business logic may be written based on the values of this field.
     * 
     */
    @JsonProperty("serverStatusCode")
    private String serverStatusCode;
    /**
     * API Status Severity Code.
     * Valid values:
     * Error – There was an error in the request severe enough that no response could be made. The requested transaction was not processed.
     * Warning – There was a problem with the request, but a valid response is still present. The requested transaction was processed.
     * Info – Information Only. The requested transaction was processed.
     * (Required)
     * 
     */
    @JsonProperty("severity")
    private AdditionalStatus.AddSeverity addSeverity;

    /**
     * Status Description. Explanatory text associated with a status such as the response status code, the bank account status, stopped check status or other related object status.
     * 
     */
    @JsonProperty("statusDesc")
    private String statusDesc;

    /**
     * No args constructor for use in serialization
     * 
     */
    public AdditionalStatus()
    {
    }

    /**
     * 
     * @param statusCode
     * @param serverStatusCode
     * @param addSeverity
     * @param statusDesc
     */
    public AdditionalStatus(Integer statusCode, String serverStatusCode, AdditionalStatus.AddSeverity addSeverity,
	    String statusDesc)
    {
	this.statusCode = statusCode;
	this.serverStatusCode = serverStatusCode;
	this.addSeverity = addSeverity;
	this.statusDesc = statusDesc;
    }

    public Integer getStatusCode()
    {
	return statusCode;
    }

    public void setStatusCode(Integer statusCode)
    {
	this.statusCode = statusCode;
    }

    public String getServerStatusCode()
    {
	return serverStatusCode;
    }

    public void setServerStatusCode(String serverStatusCode)
    {
	this.serverStatusCode = serverStatusCode;
    }

    public AdditionalStatus.AddSeverity getSeverity()
    {
	return addSeverity;
    }

    public void setSeverity(AdditionalStatus.AddSeverity addSeverity)
    {
	this.addSeverity = addSeverity;
    }

    public String getStatusDesc()
    {
	return statusDesc;
    }

    public void setStatusDesc(String statusDesc)
    {
	this.statusDesc = statusDesc;
    }

    public enum AddSeverity
    {

	INFO("Info"), WARNING("Warning"), ERROR("Error");
	private final String value;
	private final static Map<String, AdditionalStatus.AddSeverity> CONSTANTS = new HashMap<String, AdditionalStatus.AddSeverity>();

	static
	{
	    for (AdditionalStatus.AddSeverity c : values())
	    {
		CONSTANTS.put(c.value, c);
	    }
	}

	private AddSeverity(String value)
	{
	    this.value = value;
	}

	@JsonValue
	@Override
	public String toString()
	{
	    return this.value;
	}

	@JsonCreator
	public static AdditionalStatus.AddSeverity fromValue(String value)
	{
	    AdditionalStatus.AddSeverity constant = CONSTANTS.get(value);
	    if (constant == null)
	    {
		throw new IllegalArgumentException(value);
	    } else
	    {
		return constant;
	    }
	}
    }
}
