package com.ad.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonValue;

/**
 * Status.
 * The error reporting structure that may be returned by an API call.
 * 
 */

@XmlRootElement
@XmlType(name = "Status", namespace="httpstatus")
public class Status implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * HTTP Server Status Code.
     * The value placed here is used to allow the client to display a status code to the user, or log appropriately for troubleshooting purposes.
     * No business logic may be written based on the values of this field.
     * (Required)
     * 
     */
    @JsonProperty("serverStatusCode")
    private String serverStatusCode;

    @JsonProperty("severity")
    private Status.Severity severity;

    @JsonProperty("AdditionalStatuss")
    private List<AdditionalStatus> AdditionalStatuss = new ArrayList<AdditionalStatus>();

    /**
     * No args constructor for use in serialization
     * 
     */
  
    public Status()
    {
	List<AdditionalStatus> additionalStatusList=new ArrayList<AdditionalStatus>();
	AdditionalStatus additionalStatus=new AdditionalStatus();
	additionalStatus.setSeverity(AdditionalStatus.AddSeverity.ERROR);
	additionalStatus.setStatusCode(400);
	additionalStatusList.add(additionalStatus);
	this.serverStatusCode = "400";
	this.severity = Status.Severity.ERROR;
	this.setAdditionalStatuss(additionalStatusList);
    }
    

    /**
     * 
     * @param serverStatusCode
     * @param severity
     * @param AdditionalStatuss
     */
    public Status(String serverStatusCode, Status.Severity severity, List<AdditionalStatus> AdditionalStatuss)
    {
	this.serverStatusCode = serverStatusCode;
	this.severity = severity;
	this.AdditionalStatuss = AdditionalStatuss;
    }

    /**
     * HTTP Server Status Code.
     * The value placed here is used to allow the client to display a status code to the user, or log appropriately for troubleshooting purposes.
     * No business logic may be written based on the values of this field.
     * (Required)
     * 
     * @return
     *     The serverStatusCode
     */
    @JsonProperty("serverStatusCode")
    public String getServerStatusCode()
    {
	return serverStatusCode;
    }

    /**
     * HTTP Server Status Code.
     * The value placed here is used to allow the client to display a status code to the user, or log appropriately for troubleshooting purposes.
     * No business logic may be written based on the values of this field.
     * (Required)
     * 
     * @param serverStatusCode
     *     The serverStatusCode
     */
    @JsonProperty("serverStatusCode")
    public void setServerStatusCode(String serverStatusCode)
    {
	this.serverStatusCode = serverStatusCode;
    }

    public Status withServerStatusCode(String serverStatusCode)
    {
	this.serverStatusCode = serverStatusCode;
	return this;
    }


    @JsonProperty("severity")
    public Status.Severity getSeverity()
    {
	return severity;
    }


    @JsonProperty("severity")
    public void setSeverity(Status.Severity severity)
    {
	this.severity = severity;
    }

    public Status withSeverity(Status.Severity severity)
    {
	this.severity = severity;
	return this;
    }


    @JsonProperty("AdditionalStatuss")
    public List<AdditionalStatus> getAdditionalStatuss()
    {
	if(null == AdditionalStatuss)
	{
	    AdditionalStatuss = new ArrayList<AdditionalStatus>();
	}
	return AdditionalStatuss;
    }

    @JsonProperty("AdditionalStatuss")
    public void setAdditionalStatuss(List<AdditionalStatus> AdditionalStatuss)
    {
	this.AdditionalStatuss = AdditionalStatuss;
    }

    public Status withAdditionalStatuss(List<AdditionalStatus> AdditionalStatuss)
    {
	this.AdditionalStatuss = AdditionalStatuss;
	return this;
    }

    @Override
    public String toString()
    {
	return ToStringBuilder.reflectionToString(this);
    }

    public enum Severity
    {
	WARNING("Warning"), ERROR("Error");
	private final String value;
	private final static Map<String, Status.Severity> CONSTANTS = new HashMap<String, Status.Severity>();

	static
	{
	    for (Status.Severity c : values())
	    {
		CONSTANTS.put(c.value, c);
	    }
	}

	private Severity(String value)
	{
	    this.value = value;
	}

	@JsonValue
	@Override
	public String toString()
	{
	    return this.value;
	}

	@JsonCreator
	public static Status.Severity fromValue(String value)
	{
	    Status.Severity constant = CONSTANTS.get(value);
	    if (constant == null)
	    {
		throw new IllegalArgumentException(value);
	    } else
	    {
		return constant;
	    }
	}

    }

}
