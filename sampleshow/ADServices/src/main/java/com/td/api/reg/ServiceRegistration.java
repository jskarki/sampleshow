package com.td.api.reg;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.ad.api.ADService;

public class ServiceRegistration extends Application
{
    private Set<Object> singletons = new HashSet<Object>();

    public ServiceRegistration()
    {
	singletons.add(new ADService());
    }

    @Override
    public Set<Object> getSingletons()
    {
	return singletons;
    }
}
