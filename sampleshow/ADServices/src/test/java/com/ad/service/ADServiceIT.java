package com.ad.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.core.api.annotation.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.ad.model.AdDetailJson;
import com.ad.model.AdListJson;
import com.ad.util.CommonUtil;
import com.ad.util.JSONMapperUtil;

/**
 * This class is the integration test. For these to rum make sure to deploy the services on the server.
 * @author JKARKI
 *
 */
@RunWith(Arquillian.class)
public class ADServiceIT
{
    @Deployment
    public static JavaArchive createDeployment()
    {
	return ShrinkWrap.create(JavaArchive.class).addClasses(AdListJson.class, AdDetailJson.class).addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Inject
    AdListJson adsList;
    @Inject
    AdDetailJson ad1Partner1;
    @Inject
    AdDetailJson ad2Partner1;
    @Inject
    AdDetailJson ad1Partner2;

    @Test
    public void testCreateAds()
    {
	try
	{
	    ad1Partner1 = new AdDetailJson("Partner1", 60, "Add content for ad one partner1.");
	    ad2Partner1 = new AdDetailJson("Partner1", 70, "Add content for ad two partner2.");
	    ad1Partner2 = new AdDetailJson("Partner2", 80, "Add content for ad one partner2 ");
	    
	    //Make sure the service API is deployed and running on the Web/App Server
	    String URL = "http://localhost:8080/ADServer/RS/adservice/createAd";
	    String requestJson = JSONMapperUtil.convertJSONToString(ad1Partner1, true);
	    
	    //Post first Ad
	    CommonUtil.sendPOST(URL, requestJson);
	    
	    //Post second Ad
	    requestJson = JSONMapperUtil.convertJSONToString(ad2Partner1, true);
	    CommonUtil.sendPOST(URL, requestJson);
	    
	    //Post third Ad
	    requestJson = JSONMapperUtil.convertJSONToString(ad1Partner2, true);
	    String finalResult = CommonUtil.sendPOST(URL, requestJson);
	    
	    adsList = JSONMapperUtil.convertStringTOJSON(finalResult, AdListJson.class, true);

	    assertNotNull(adsList);
	    
	    // Results should be greater than zero, not asserting exact match as the number of ads will vary depending on how many times
	    // The project is build with test and how many http calls are made to the service API method.
	    assertTrue(adsList.getAdListMap().size() > 0);
	} catch (Exception ex)
	{
	    ex.printStackTrace();
	}
    }
    
    @Test
    public void testGetAllAds()
    {
	try
	{
	    //Make sure the service API is deployed and running on the Web/App Server
	    String URL = "http://localhost:8080/ADServer/RS/adservice/getAllAds";
	    String finalResult = CommonUtil.getCall(URL);
	    
	    adsList = JSONMapperUtil.convertStringTOJSON(finalResult, AdListJson.class, true);

	    assertNotNull(adsList);
	    
	    // Results should be greater than zero, not asserting exact match as the number of ads will vary depending on how many times
	    // The project is build with test and how many http calls are made to the service API method.
	    assertTrue(adsList.getAdListMap().size() > 0);
	} catch (Exception ex)
	{
	    ex.printStackTrace();
	}
    }
    
    @Test
    public void testGetAdsForPartner()
    {
	try
	{
	    //Make sure the service API is deployed and running on the Web/App Server
	    String URL = "http://localhost:8080/ADServer/RS/adservice/getAdsByPartner/Partner2";
	    String finalResult = CommonUtil.getCall(URL);
	    
	    adsList = JSONMapperUtil.convertStringTOJSON(finalResult, AdListJson.class, true);

	    assertNotNull(adsList);
	    
	    // Results should be greater than zero, not asserting exact match as the number of ads will vary depending on how many times
	    // The project is build with test and how many http calls are made to the service API method.
	    assertTrue(adsList.getAdListMap().size() > 0);
	    System.out.println(finalResult);
	    
	} catch (Exception ex)
	{
	    ex.printStackTrace();
	}
    }
}