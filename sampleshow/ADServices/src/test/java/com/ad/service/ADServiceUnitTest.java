package com.ad.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ad.api.ADServiceHandler;
import com.ad.model.AdDetailJson;
import com.ad.model.AdListJson;

@RunWith(MockitoJUnitRunner.class)
public class ADServiceUnitTest
{
    private static ADServiceHandler mockedADServiceHandler;
    private static ADServiceHandler adServiceHandler;

    private static AdListJson adListJson = new AdListJson();
    private static AdDetailJson ad1Partner1;
    private static AdDetailJson ad1Partner2;
    private static AdDetailJson ad2Partner1;
    private static AdDetailJson ad2Partner2;
    private static AdDetailJson ad1Partner3;
    private static AdDetailJson ad2Partner3;
    private static AdDetailJson ad1Partner4;

    /**
     * Setup any test data here for this class test cases.
     */
    @BeforeClass
    public static void setUp()
    {
	mockedADServiceHandler = mock(ADServiceHandler.class);

	ad1Partner1 = new AdDetailJson("Partner1", 55, "Add content1");
	ad1Partner2 = new AdDetailJson("Partner1", 50, "Add content2");
	ad2Partner1 = new AdDetailJson("Partner2", 60, "Add content1");
	ad2Partner2 = new AdDetailJson("Partner2", 65, "Add content2");

	ArrayList<AdDetailJson> adList1 = new ArrayList<AdDetailJson>();

	adList1.add(ad1Partner1);
	adList1.add(ad1Partner2);

	ArrayList<AdDetailJson> adList2 = new ArrayList<AdDetailJson>();
	adList2.add(ad2Partner1);
	adList2.add(ad2Partner2);

	adListJson.getAdListMap().put("Partner1", adList1);
	adListJson.getAdListMap().put("Partner2", adList2);

	when(mockedADServiceHandler.manageAds(adListJson, ad1Partner1)).thenReturn(adListJson);
	when(mockedADServiceHandler.getAllAds(adListJson)).thenReturn(adListJson);
	when(mockedADServiceHandler.getAdsByPartner(adListJson, "Partner1")).thenReturn(adList1);

    }

    /**
     * With mock this method tests creates the ad's.
     * @throws Exception
     */
    @Test
    public void testCreateAd() throws Exception
    {
	AdListJson adList = mockedADServiceHandler.manageAds(adListJson, ad1Partner1);
	assertEquals(2, adList.getAdListMap().get("Partner1").size());
    }

    /**
     * With mock this method tests to get all the ad till date in the list and check the list by partner id.
     * @throws Exception
     */
    @Test
    public void testGetAllAds() throws Exception
    {
	AdListJson adList = mockedADServiceHandler.getAllAds(adListJson);
	assertEquals(2, adList.getAdListMap().get("Partner1").size());
	assertEquals(2, adList.getAdListMap().get("Partner2").size());
    }

    /**
     * With mock this method tests to get the ad list for the specified partner id and verifies the number of ad for that partner.
     * @throws Exception
     */
    @Test
    public void testGetAdsByPartner() throws Exception
    {
	ArrayList<AdDetailJson> partnerAds = mockedADServiceHandler.getAdsByPartner(adListJson, "Partner1");
	assertEquals(2, partnerAds.size());
    }

    /**
     * Without mock this method tests creates 1 ad each for two partners and also check the number of ads for other 
     * partners created before in the test setup.
     * @throws Exception
     */
    @Test
    public void testCreateAdWithoutMock() throws Exception
    {
	ad1Partner3 = new AdDetailJson("Partner3", 30, "Add content1 partner 3");
	ad1Partner4 = new AdDetailJson("Partner4", 40, "Add content1 partner 4");

	AdListJson adList = adServiceHandler.manageAds(adListJson, ad1Partner3);
	adList = adServiceHandler.manageAds(adListJson, ad1Partner4);
	assertEquals(4, adList.getAdListMap().size());
	assertEquals(2, adList.getAdListMap().get("Partner1").size());
	assertEquals(1, adList.getAdListMap().get("Partner3").size());
    }

    /**
     * Without mock this method tests to get all the ad till date in the list and check the list by partner id.
     * @throws Exception
     */
    @Test
    public void testGetAllAdsWithoutMock() throws Exception
    {
	adServiceHandler = new ADServiceHandler();
	AdListJson adList = adServiceHandler.getAllAds(adListJson);
	assertEquals(2, adList.getAdListMap().get("Partner1").size());
	assertEquals(2, adList.getAdListMap().get("Partner2").size());
    }

    /**
     * Without mock this method tests to get the ad list for the specified partner id and verifies the number of ad for that partner.
     * @throws Exception
     */
    @Test
    public void testGetAdsByPartnerWithoutMock() throws Exception
    {
	ArrayList<AdDetailJson> partnerAds = adServiceHandler.getAdsByPartner(adListJson, "Partner1");
	assertEquals(2, partnerAds.size());
    }

    /**
     * Without mock this method tests that 2 ads were added to the ad list for the same partner and only one ad is active.
     * @throws Exception
     */
    @Test
    public void testOnlyOneAactiveAdWithoutMock() throws Exception
    {
	ad1Partner3 = new AdDetailJson("Partner3", 30, "Add content1 partner 3");
	ad2Partner3 = new AdDetailJson("Partner3", 35, "Add content2 partner 3");
	ad1Partner4 = new AdDetailJson("Partner4", 40, "Add content1 partner 4");

	AdListJson adList = adServiceHandler.manageAds(adListJson, ad1Partner3);
	adList = adServiceHandler.manageAds(adListJson, ad1Partner4);
	adList = adServiceHandler.manageAds(adListJson, ad2Partner3);

	int activeAdcount = 0;
	for (AdDetailJson ad : adList.getAdListMap().get("Partner3"))
	{
	    if (ad.isAd_active())
	    {
		activeAdcount++;
	    }
	}

	assertEquals(1, activeAdcount);
    }

}