
The Server exposes three API as mentioned below:

1. createAd
This method will return create the Ad in a single HashMap in the memory.

(Example)

Request Type : POST
URL			 : http://localhost:8080/ADServer/RS/adservice/createAd
Headers		 :

accept: application/json
X-KIE-ContentType: JSON
Content-type: application/json

Payload:

{
	"Partner_id": "Partner1",
	"Duration_secs": 50,
	"Ad_content": "New AD for partner 1."
}

2. getAllAds
This API method will return the JSON list of all the Ad's.

(Example)

Request Type : GET
URL			 : http://localhost:8080/ADServer/RS/adservice/getAllAds
Headers		 :

accept: application/json
X-KIE-ContentType: JSON
Content-type: application/json

Response:

{
	"AdListMap": {
		"PartnerNew2": [{
			"Partner_id": "PartnerNew2",
			"Duration_secs": 50,
			"Ad_content": "New AD Who can do this program? Mine",
			"Ad_active": true,
			"AdStartTime": 1494190955410
		},
		{
			"Partner_id": "PartnerNew2",
			"Duration_secs": 50,
			"Ad_content": "New AD Who can do this program? New",
			"Ad_active": false,
			"AdStartTime": 1494190965342
		}],
		"PartnerNew1": [{
			"Partner_id": "PartnerNew1",
			"Duration_secs": 50,
			"Ad_content": "New AD Who can do this program? New",
			"Ad_active": true,
			"AdStartTime": 1494190979662
		}],
		
	}
}


3. getAdsByPartner

This method will return all Ad's for the requested partner.

(Example)

Request Type : GET
URL			 : http://localhost:8080/ADServer/RS/adservice/getAdsByPartner/{partnername}
Headers		 :

accept: application/json
X-KIE-ContentType: JSON
Content-type: application/json

Response:

[{
	"Partner_id": "PartnerNew2",
	"Duration_secs": 50,
	"Ad_content": "New AD Who can do this program? Mine",
	"Ad_active": true,
	"AdStartTime": 1494190955410
},
{
	"Partner_id": "PartnerNew2",
	"Duration_secs": 50,
	"Ad_content": "New AD Who can do this program? New",
	"Ad_active": false,
	"AdStartTime": 1494190965342
}]


Other Information:

* This project is ceated as MAVEN based project.
* Build WAR file can be deployed to any application server. Tested on JBoss EAP 6.4+.
* Junit and Integration test results can be execute while building the project or once the project is compiled then run as Junit test.
* Test each API method with example details as mentioned in the above section.

* JUNIT - ADServiceUnitTest.java
* Integration Test - ADServiceIT.java 
* To test and example of the exception from REST Client, remove Partner_id field from the payload JSON for POST createAd.

Other Assumptions or Facts:

* Currenlty the once the ad is expired, there no method to activate a particulr ad, project can be easliy enahnaced for the same and any other freatures required.
* Ads are added to working memory object the running HTTP server, once server restarts then all data will be lost.
* This application can bbe enahnced for the persisitence of the ads.
